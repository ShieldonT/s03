year = input("Please input a year \n")

if year.isalpha() or int(year) <=0:
    print("Invalid input")
elif int(year) % 4 == 0:
    print(f"{year} is a leap year")
else:
    print(f"{year} is not a leap year")

rows = int(input("Enter number of rows \n"))
columns = int(input("Enter number of columns \n"))
for x in range(rows):
    print("*"*columns)

